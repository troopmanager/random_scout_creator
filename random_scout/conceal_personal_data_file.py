import csv
from pathlib import Path

from random_scout import Scout
from random import randint


def replace_data(line, scout):
    line["BSA Member ID"] = scout.bsa_id
    line["First Name"] = scout.first_name
    line["Last Name"] = scout.last_name
    line["Suffix"] = ""
    line["Nickname"] = ""
    line["Address 1"] = scout.address_1
    line["Address 2"] = ""
    line["City"] = scout.city
    line["State"] = scout.state
    line["Zip"] = scout.zip
    line["Home Phone"] = scout.phone_number
    line["School Grade"] = randint(5, 12)
    line["School Name"] = "Bovine University"
    line["LDS"] = ""
    line["Swimming Classification"] = ""
    line["Swimming Classification Date"] = ""
    line["Unit Number"] = randint(2, 9999)
    line["Patrol Name"] = scout.patrol_name
    line["Parent 1 Email"] = scout.parent_email_1
    line["Parent 2 Email"] = scout.parent_email_2


with open(Path("data") / "input" / "troop_162_B__scouts.csv") as f:
    with open(Path("data") / "output" / "Output_personal_data.csv", "w") as g:
        reader = csv.DictReader(f)
        writer = csv.DictWriter(
            g, quoting=csv.QUOTE_ALL, fieldnames=list(reader.fieldnames)
        )
        writer.writeheader()

        for line in reader:
            replace_data(line, Scout.get_random())
            writer.writerow(line)
