import csv
from pathlib import Path
from random import randint
from pprint import pprint

from random_scout import Scout

scouts = {}


def change_if_exists(field, data, new_value):
    if field in data:
        if data[field]:
            data[field] = new_value


def replace_advancement_data(line, scout):
    line["BSA Member ID"] = scout.bsa_id
    line["First Name"] = scout.first_name
    line["Middle Name"] = scout.middle_name
    line["Last Name"] = scout.last_name
    change_if_exists(
        "MarkedCompletedBy",
        line,
        f"{Scout.get_random().first_name} {Scout.get_random().last_name}",
    )
    change_if_exists(
        "CounselorApprovedBy",
        line,
        f"{Scout.get_random().first_name} {Scout.get_random().last_name}",
    )

    """ For some reason, the BSA scoutbook export has the columns reversed for merit badge reuqirements
    """
    if line["Advancement Type"] == "Merit Badge Requirement":
        change_if_exists(
            "LeaderApprovedDate",
            line,
            f"{Scout.get_random().first_name} {Scout.get_random().last_name}",
        )
    else:
        change_if_exists(
            "LeaderApprovedBy",
            line,
            f"{Scout.get_random().first_name} {Scout.get_random().last_name}",
        )
    change_if_exists(
        "AwardedBy",
        line,
        f"{Scout.get_random().first_name} {Scout.get_random().last_name}",
    )

    return line


def replace_personal_data(line, scout):
    line["BSA Member ID"] = scout.bsa_id
    line["First Name"] = scout.first_name
    line["Last Name"] = scout.last_name
    line["Suffix"] = ""
    line["Nickname"] = ""
    line["Address 1"] = scout.address_1
    line["Address 2"] = ""
    line["City"] = scout.city
    line["State"] = scout.state
    line["Zip"] = scout.zip
    line["Home Phone"] = scout.phone_number
    line["School Grade"] = randint(5, 12)
    line["School Name"] = "Bovine University"
    line["LDS"] = ""
    line["Swimming Classification"] = ""
    line["Swimming Classification Date"] = ""
    line["Unit Number"] = randint(2, 9999)
    line["Patrol Name"] = scout.patrol_name
    line["Parent 1 Email"] = scout.parent_email_1
    line["Parent 2 Email"] = scout.parent_email_2
    return line


with open(Path("data") / "input" / "troop_162_B__scouts.csv") as input_personal, open(
    Path("data") / "output" / "Output_personal_data.csv", "w"
) as output_personal:
    reader = csv.DictReader(input_personal)
    writer = csv.DictWriter(
        output_personal, quoting=csv.QUOTE_ALL, fieldnames=list(reader.fieldnames)
    )
    writer.writeheader()
    for line in reader:
        bsa_id = line["BSA Member ID"]
        scouts[bsa_id] = Scout.get_random()
        line = replace_personal_data(line, scouts[bsa_id])
        writer.writerow(line)


with (
    open(Path("data") / "input" / "troop_162_B__advancement.csv") as input_advancement,
    open(
        Path("data") / "output" / "Output_advancement_data.csv", "w"
    ) as output_advancement,
):
    advancement_reader = csv.DictReader(input_advancement)
    advancement_writer = csv.DictWriter(
        output_advancement,
        fieldnames=list(advancement_reader.fieldnames),
    )
    advancement_writer.writeheader()

    for line in advancement_reader:
        print(len(line))
        if None in line:
            del line[None]
        bsa_id = line["BSA Member ID"]
        if bsa_id in scouts:
            s = scouts[bsa_id]
        else:
            scouts[bsa_id] = Scout.get_random()

        line = replace_advancement_data(line, s)

        advancement_writer.writerow(line)
