import random
from pathlib import Path
from datetime import date

import phonenumbers
import faker
from faker.providers.phone_number.en_US import Provider


# Fake Data needed: (2 hashes means "Done")
## BSA Member ID (8-digit int between 10000000 and 20000000)
## First Name
## Middle Name
## Last Name
# Suffix (Optional)
# Nickname (Optional)
## Address 1 (street address only)
# Address 2 (Optional)
## City
## State
## Zip (5 digit int)
## Home Phone (in "(xxx) xxx-xxxx" format)
# School Grade (int between 5 and 12)
# School name
# Unit Number (4 digit int)
# Date joined scouts bsa (date within last 5 years in yyyy-mm-dd format)
## Patrol name
# parent 1 email
# parent 2 email (optional)
# parent 3 email (optional)


scout_faker = faker.Faker()

BASE = Path(__file__).parent.parent / "data"


with open(BASE / "input" / "patrol_name_first_part.txt") as f:
    patrol_first_names = [name.strip() for name in f]


with open(BASE / "input" / "patrol_name_second_part.txt") as f:
    patrol_second_names = [name.strip() for name in f]


class PatrolNameProvider(Provider):
    def patrol_name(self):
        return (
            f"{random.choice(patrol_first_names)} {random.choice(patrol_second_names)}"
        )


scout_faker.add_provider(PatrolNameProvider)


# from this stackoverflow:
# https://stackoverflow.com/questions/36479550/python-how-to-generate-a-random-phone-number
class CustomPhoneProvider(Provider):
    def custom_phone_number(self):
        while True:
            phone_number = self.numerify(self.random_element(self.formats))
            parsed_number = phonenumbers.parse(phone_number, "US")
            if phonenumbers.is_valid_number(parsed_number):
                formatted_number = phonenumbers.format_number(
                    parsed_number, phonenumbers.PhoneNumberFormat.E164
                )
                return f"({formatted_number[2:5]}) {formatted_number[5:8]}-{formatted_number[8:12]}"


scout_faker.add_provider(CustomPhoneProvider)


class BSAIDProvider(Provider):
    def bsa_id(self):
        return random.randint(10000000, 20000000)


scout_faker.add_provider(BSAIDProvider)


class BirthdateAgeProvider(Provider):
    def birthdate_and_age(self):
        birthdate = scout_faker.date_between(start_date="-18y", end_date="-11y")
        age = date.today() - birthdate
        return birthdate, int(age.days / 365)


scout_faker.add_provider(BirthdateAgeProvider)


class Scout:
    def __init__(self):
        pass

    def __str__(self):
        return f"""\
{self.last_name}, {self.first_name} \
{self.patrol_name}
Birthdate: {self.birthdate.strftime("%Y-%m-%d")} \
Age: {self.age}
BSA ID: {self.bsa_id} \
Phone: {self.phone_number}\
"""

    @classmethod
    def get_random(cls):
        self = Scout()
        self.first_name = scout_faker.first_name()
        self.middle_name = scout_faker.first_name()
        self.last_name = scout_faker.last_name()
        self.patrol_name = scout_faker.patrol_name()
        self.birthdate, self.age = scout_faker.birthdate_and_age()
        self.bsa_id = scout_faker.bsa_id()
        self.phone_number = scout_faker.custom_phone_number()
        self.address_1 = scout_faker.street_address()
        self.city = scout_faker.city()
        self.zip = scout_faker.postcode()
        self.state = scout_faker.state_abbr()
        self.parent_email_1 = scout_faker.email()
        self.parent_email_2 = scout_faker.email()

        return self


if __name__ == "__main__":
    s = Scout.get_random()
    print(f"{s.last_name}, {s.first_name} {s.middle_name}")
    print(s.patrol_name)
    print(f'Birthdate: {s.birthdate.strftime("%Y-%m-%d")}')
    print(f"Age: {s.age}")
    print(f"BSA ID: {s.bsa_id}")
    print(f"Phone: {s.phone_number}")
    print(f"Address: {s.address_1} {s.city}, {s.state} {s.zip}")
    print(f"Parent email: {s.parent_email_1}")
    print(f"Parent email 2: {s.parent_email_2}")
